import nltk
from nltk.corpus import wordnet as wn, wordnet_ic
from nltk.corpus import stopwords
from rake_nltk import Rake

f = open("../Resource/text_lect1.txt")
data = f.read()
sentences = nltk.sent_tokenize(data)
brown_ic = wordnet_ic.ic('ic-brown.dat')
r = Rake()
	
def preprocess(sentences):
	output_sentences = []
	for sentence in sentences:
		flag = True
		for ch in sentence:
			try:
				ch = int(ch)
			except:
				continue
			if( ( ch >= 65 and ch <= 90 ) or ( ch >= 98 or ch <= 122)):
				flag = False
				break
		if(flag == True):
			output_sentences.append(sentence)
	return output_sentences
	
sentences = preprocess(sentences)

#Presence of word "algorithm" and NER
def f1_feature(sentences):
	output_sentences = []
	for sentence in sentences:
		word_bag = nltk.word_tokenize(sentence)
		flag = False
		for word in word_bag:
			if(word == "course"):
				flag = True
		if(flag == True):
			r.extract_keywords_from_text(sentence)
			print(r.get_ranked_phrases_with_scores()[0])
		
			
f1_feature(sentences)				
