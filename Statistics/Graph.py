import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords  
import csv
import pandas as pd

class syntax_graph:

	def __init__(self, filename):
		FILE = open(filename, "r")
		text = FILE.read()
#		text = "my name is  siddhesh."
		data = nltk.sent_tokenize(text)
		for i in range(0, len(data)):
			if(len(data[i]) == 1):
				data.pop(i)
		raw_tokens = list(set(nltk.word_tokenize(text)))
		temp_word_tokens = []
		self.word_tokens = []
		self.special_char = set(['\'', ';','\"', '`', '!', '@', '#', '$', '%', '^','&', '*', '(', ')', '\\', '[',']', '{', '}', ',', '``', '.'])
		for token in raw_tokens:
#			if token not in self.special_char:
			temp_word_tokens.append(token)		
		
		lemmatizer = WordNetLemmatizer()
		for i in range(0, len(temp_word_tokens)):
			temp_word_tokens[i] = lemmatizer.lemmatize(temp_word_tokens[i])
			if(temp_word_tokens[i].isupper() != True):
				temp_word_tokens[i] = temp_word_tokens[i].lower() 
				
		#storing original word in document for lemmetized word
		PATH = '/tmp/graph_box.txt'	
		temp_file = open(PATH, "w")
		print(len(raw_tokens), len(temp_word_tokens))
		for i in range(0, 245):	
			temp_file.write(raw_tokens[i] + " " +  temp_word_tokens[i]+ "\n")
			
		for token in temp_word_tokens:
			if token not in self.special_char:
				self.word_tokens.append(token)
		
		
		self.nodes = {}
		self.edges = {}
		self.data = data
		
		
	def set_data(self,data):
		self.data = data
	
	def set_tokens(self, tokens):
		self.tokens = tokens
		
	def get_nodes(self):
		return self.nodes
		
	def initialize(self, tokens = None, data = None):
		
		if(tokens != None):
			self.set_tokens(tokens)
		
		if(data != None):
			self.set_data(data)
			
		for token in self.word_tokens:
			node = Node(token)
			self.nodes[token] = node
			
		for parent in self.nodes.keys():
			self.edges[parent] = {}
			for child in self.nodes.keys():
				self.edges[parent][child] = 0
		
		
	def build(self):
		
		lemmatizer = WordNetLemmatizer()
		n = len(self.data)
		for i in range(0, n):
			sentence = self.data[i]
			word_bag = nltk.word_tokenize(sentence)
			wordList = []
			for j in range(0, len(word_bag)):
				word_bag[j] = lemmatizer.lemmatize(word_bag[j])
				if(word_bag[j].isupper() != True):
					word_bag[j] = word_bag[j].lower() 
				if word_bag[j] in self.word_tokens:
					wordList.append(word_bag[j])
			
			m = len(wordList)
			for j in range(0, m-1):
				parent_node_name = wordList[j]
				child_node_name = wordList[j+1]
				self.edges[parent_node_name][child_node_name] = self.edges[parent_node_name][child_node_name] + 1
				self.nodes[child_node_name].insert_parent(parent_node_name)
				self.nodes[parent_node_name].insert_child(child_node_name)	
		self.flyods_warshalls()	
			 	
	def calculate_statistics(self):
		
		for node_name in self.nodes.keys():
			if(self.nodes[node_name].get_label() != 'STOPWORD'):
				self.nodes[node_name].calculate_clustering_coefficient(self.edges)
				self.nodes[node_name].calculate_degree_centrality(self.edges)
				self.nodes[node_name].calculate_indegree_centrality(self.edges)
				self.nodes[node_name].calculate_outdegree_centrality(self.edges)
				self.nodes[node_name].calculate_strength_in(self.edges)
				self.nodes[node_name].calculate_strength_out(self.edges)
				self.nodes[node_name].calculate_strength(self.edges)
				self.nodes[node_name].calculate_selectivity(self.edges)
				self.nodes[node_name].calculate_selectivity_in(self.edges)
				self.nodes[node_name].calculate_selectivity_out(self.edges)
				self.nodes[node_name].calculate_closeness_centrality(self.A)
#				self.nodes[node_name].calculate_betweenness_centrality(self.A, self.edges, self.nodes)
				
				
	def flyods_warshalls(self):
		
		A = {}
		INT_MAX = 987654
		for parent in self.edges.keys():
			A[parent] = {}
			for child in self.edges.keys():
				if(child == parent):
					A[parent][child] = 0
				elif(self.edges[parent][child] == 0):
					A[parent][child] = INT_MAX
				else:
					A[parent][child] = self.edges[parent][child]
					
		n = len(self.edges)
		for k in self.edges.keys():
			for i in self.edges.keys():
				for j in self.edges.keys():
					if(i == j):
						continue
					elif(k == i or k == j):
						continue
					else:
						A[i][j] = min(A[i][j], A[i][k] + A[k][j]) 
		self.A = A
		return self.A
		
		
class Node:

	def __init__(self, name):
		self.name = ""
		self.pos_tag = ""
		self.label = ""
		self.afterList = set()
		self.beforeList = set()
		self.indegree = 0
		self.outdegree = 0
		if(name != None):
			self.set_name(name)
			self.set_pos_tag(name)
			self.set_label(name)
			
		self.clustering_coefficient = 0
		self.degree_centrality = 0
		self.indegree_centrality = 0
		self.outdegree_centrality = 0
		self.strength = 0
		self.strength_in = 0
		self.strength_out = 0
		self.selectivity = 0
		self.selectivity_in = 0
		self.selectivity_out = 0
		self.closeness_centrality = 0
		self.betweenness_centrality = 0
			
	def get_node_info(self):
		info = {}
		info['name'] = self.name
		info['label'] = self.label
		info['pos_tag'] = self.pos_tag
		info['clustering_coefficient'] = self.clustering_coefficient
		info['degree_centrality'] = self.degree_centrality
		info['indegree_centrality'] = self.indegree_centrality
		info['ourdegree_centrality'] = self.outdegree_centrality
		info['strength'] = self.strength
		info['strength_in'] = self.strength_in
		info['strength_out'] = self.strength_out
		info['selectivity'] = self.selectivity
		info['selectivity_in'] = self.selectivity_in
		info['selectivity_out'] = self.selectivity_out
		info['closeness_centrality'] = self.closeness_centrality
		info['betweenness_centrality'] = self.betweenness_centrality
		return info						
			
	def set_name(self, word):
		self.name = word
		
	def get_name(self):
		return self.name
		
	def set_pos_tag(self, word):
		
		if(word != None):
			self.pos_tag = nltk.pos_tag([word])[0][1]
		else:
			self.pos_tag = nltk.pos_tag([self.name])[0][1]
			
	def get_pos_tag(self):
		return self.pos_tag
			
	def set_label(self, word):
		
		if(word == None):
			word = self.name
			
		stop_words = set(stopwords.words('english'))
		if self.name in stop_words:
			self.label = 'STOPWORD'
		else:
			self.label = "DELIMITER"
			
	def get_label(self):
		return self.label

	def insert_parent(self, node_name):
		self.beforeList.add(node_name)
	
	def insert_child(self, node_name):
		self.afterList.add(node_name)
		
	def get_indegree(self):
		self.indegree = len(self.beforeList)
		return self.indegree
	
	def get_outdegree(self):
		self.outdegree = len(self.afterList)
		return self.outdegree
		
	def get_degree(self):
		return self.get_indegree() + self.get_outdegree()

	def get_afterList(self):
		return self.afterList
		
	def get_beforeList(self):
		return self.beforeList	
		
	def calculate_clustering_coefficient(self, edges):
		
		neighbours = list(self.get_beforeList()) + list(self.get_afterList())
		Nv = len(neighbours)
		if(Nv == 0 or Nv == 1):
			self.clustering_coefficient = 0
			return 0
			
		Ev = 0
		for neighbour1 in neighbours:
			for neighbour2 in neighbours:
				Ev = Ev + edges[neighbour1][neighbour2]
				
		self.clustering_coefficient = (2*Ev)/(Nv*(Nv-1))
		return self.clustering_coefficient
		
	def calculate_degree_centrality(self, edges):
		
		dv = self.get_degree()
		N = len(edges)
		if(N == 1):
			self.degree_centrality = 1
			return 1
		self.degree_centrality = dv/(N-1)
		return self.degree_centrality
		
	def calculate_indegree_centrality(self, edges):
		
		dv_in = self.get_indegree()
		N = len(edges)
		if(N == 1):
			self.indegree_centrality = 1
			return 1
		self.indegree_centrality = dv_in/(N-1)
		return self.indegree_centrality
	
	def calculate_outdegree_centrality(self, edges):
		
		dv_out = self.get_outdegree()
		N = len(edges)
		if(N == 1):
			self.outdegree_centrality = 1
			return 1
		self.outdegree_centrality = dv_out/(N-1)
		return self.outdegree_centrality
		
	def calculate_strength_in(self, edges):
		
		for node_name in self.beforeList:
			self.strength_in = self.strength_in + edges[node_name][self.name]
		return self.strength_in
		
	def calculate_strength_out(self, edges):
		
		for node_name in self.afterList:
			self.strength_out = self.strength_out + edges[self.name][node_name]
		return self.strength_out
		
	def calculate_strength(self, edges):
		
		self.strength =  self.calculate_strength_in(edges) + self.calculate_strength_out(edges)
		return self.strength
		
	def calculate_selectivity(self,edges):
		if(self.get_degree() == 0):
			self.selectivity = 0
			return 0
		self.selectivity = self.calculate_strength(edges)/self.get_degree()
		return self.selectivity
		
	def calculate_selectivity_in(self, edges):
		
		if(self.get_indegree() == 0):
			self.selectivity_in = 0
			return 0
		self.selectivity_in = self.calculate_strength_in(edges)/self.get_indegree()
		return self.selectivity_in
		
	def calculate_selectivity_out(self, edges):
		
		if(self.get_outdegree() == 0):
			self.selectivity_out = 0
			return 0
		self.selectivity_out = self.calculate_strength_out(edges)/self.get_outdegree()
		return self.selectivity_out
		
	def calculate_closeness_centrality(self, A):
		
		d_vu = 0
		INT_MAX = 987654
		N = len(A.keys())
		for node_name in A.keys():
			d_vu = d_vu + A[self.name][node_name]
		if(d_vu > INT_MAX):
			self.closeness_centrality = 0
			return 0
		self.closeness_centrality = (N-1)/d_vu
		return self.closeness_centrality
		
	def calculate_betweenness_centrality(self, A, edges, nodes):
		f = open("temp.txt", "a")
		f.write(self.name + "\n")
		INT_MAX = 987654
		v = self.name
		ratio = 0
		for u in edges.keys():
			for t in edges.keys():
				if(u == t):
					continue
				elif(v == t or v == u):
					continue
				else:
					rho_ut_v = 0
					rho_ut = 0
					min_distance = A[u][t]
					if(min_distance >= INT_MAX):
						continue
					queue = []
					queue.append(nodes[u])
					visited = set()
					visited.add(u)
					while(len(queue) != 0):
						top_node = queue[0]
						queue.pop(0)
						childnodes = top_node.get_afterList()
						branch = -1
						for node in childnodes:
							if(A[u][node] + A[node][t] == min_distance):
								if(node != t and node not in visited):
									queue.append(nodes[node])
									visited.add(node)
								branch = branch + 1
								if(node == v):
									rho_ut_v = rho_ut_v + 1
					rho_ut = branch + 1
					if(rho_ut_v == 0):
						continue
					else:
						try:
							ratio = ratio + rho_ut_v/rho_ut
						except:
							print(rho_ut_v)
							return 		
						
		N = len(A.keys())							
		self.betweenness_centrality = 2*ratio/(N * (N - 1))		
		
		return self.betweenness_centrality

def target_keywords_graph(targetFile = "../Resource/nptl_transcript_1.txt", keyword_file = '../Resource/Graph Statistics.csv'):

#targetFile = "../Resource/nptl_transcript_1.txt"
	g = syntax_graph(targetFile)
	g.initialize()
	g.build()
	g.calculate_statistics()
	nodes = g.get_nodes()

	sample = list(nodes.keys())[0]
	field_names = list(nodes[sample].get_node_info().keys())
	graph_measures = []
	for node_name, node in nodes.items():
		graph_measures.append(node.get_node_info())

	with open(keyword_file, 'w') as csvfile:
	    writer = csv.DictWriter(csvfile, fieldnames = field_names)
	    writer.writeheader()
	    writer.writerows(graph_measures)
    
def get_target_keywords(mode = "clustering_coeffiecent"):
	df = pd.read_csv('../Resource/Graph Statistics.csv')
	df = df.sort_values(mode , ascending = False).head(50)
	words = df["name"]
	f = open("/tmp/graph_box.txt", "r")
	data = f.read().split("\n")
	f.close()
	d = {}
	for ele in data:
		try:
			ele = ele.split(" ")
			d[ele[1]] = ele[0]
		except:
			pass
	target_keywords = []
	for word in words:
		try:
			target_keywords.append(d[word])
		except:
			print(word)
	return target_keywords
	
	
	
		
