import sys
import Graph
import nltk
import word_vector
import utils
import config
import heapq


def main():
	
	
	transcript_file = sys.argv[1] 
	Graph.target_keywords_graph(transcript_file)
	target_keywords = Graph.get_target_keywords(mode = "clustering_coefficient")
	word_list = utils.get_word_list(transcript_file, config.FRAME_COUNT)
	word_list = utils.get_imp_word_list(word_list)
	vector = word_vector.word2vec(transcript_file, model = "skipgram")
	for frame in word_list[5:10]:
	
		word_vector_list = []
		for word in frame.keys():
			word_vector_list.append(vector[word])
		centroid_vector = utils.centroid(word_vector_list)
		similarity_measure = {}
		for keyword in target_keywords:
			cosine_sim = utils.cosine_similarity(centroid_vector, vector[keyword])
			similarity_measure[keyword] = cosine_sim
		print(heapq.nlargest(10, similarity_measure, key=similarity_measure.get))	
		
		
	
	
	






if __name__ == "__main__":
	main()

