import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import sklearn
import numpy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from pandas import DataFrame
import config
from numpy import dot
from numpy.linalg import norm

def getTokens(filename, stopWords = "False"):

	sampleFile = open(filename, "r")
	transcript = sampleFile.read()
	sampleFile.close()
	wordTokens = word_tokenize(transcript)
	stop_words = set(stopwords.words('english'))
	opTokens = [] 
	for r in wordTokens: 
		if(stopWords == False):
			if not r in stop_words:
				opTokens.append(r.lower())
		else:
			opTokens.append(r.lower())
	return opTokens	


def centroid(vectorList):
	centroid_vector = []
	n = len(vectorList)
	m = len(vectorList[0])
	for i in range(0, m):
		dim_i = 0
		for j in range(0, n):
			dim_i = dim_i + vectorList[j][i]
		dim_i = dim_i/n
	
		centroid_vector.append(dim_i)
	
	return centroid_vector
	
def cosine_similarity(X, Y):
	X = numpy.array(X)
	Y = numpy.array(Y)
#	cosine = sklearn.metrics.pairwise.cosine_similarity(X, Y, dense_output=True)
	cos_sim = dot(X, Y)/(norm(X)*norm(Y))
	
	return cos_sim
	
def get_word_list(filename, total_frame = 10):

	total_frames = config.FRAME_COUNT
	
	f = open(filename, "r")
	transcript = f.read()
	transcript = list(nltk.sent_tokenize(transcript))
	n = len(transcript)
	m = int(n/total_frame)
	print(m)
	if(n%total_frame != 0):
		m = m + 1
	
	word_list = []
	data = ""
	for i in range(0, n):
		if(i%m == 0):
			if(i != 0):
				word_list.append(data)
			data = ""
		data = data + transcript[i] + " "
		
	if(n%total_frame != 0):
		word_list.append(data)
	
	return word_list


def get_imp_word_list(dataset):
	tfidf_vect = TfidfVectorizer()
	doc_term_matrix = tfidf_vect.fit_transform(dataset)
	df = DataFrame(doc_term_matrix.toarray(), 
                     columns=tfidf_vect.get_feature_names())	
	words = set(df.keys()) - set(config.STOPWORDS)
	
	word_list = []
	for i in range(0, config.FRAME_COUNT):
		word_list.append({}) 
	
	for word in words:
		for i in range(0, config.FRAME_COUNT):
			if(df[word][i] > 0.0):
				word_list[i][word] = df[word][i]
	
	for i in range(0, len(word_list)):
		path = "/tmp/imp_words" + str(i) + ".txt"
		store_imp_words(word_list[i], path)
		
			
	return word_list
	
def store_imp_words(data, file_name):
	temp_file = open(file_name, "w")
	words = list(data.keys())
	tmp_data = ""
	for word in words:
		tmp_data = tmp_data + word + " "
	temp_file.write(tmp_data)
	temp_file.close()
