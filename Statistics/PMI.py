import utils as ut
import math
import nltk

targetFile = "../Resource/transcript1.txt"
w = ut.getTokens(targetFile)
w.remove(".")

def PMI_corpus(w, n = 2, head = 5):
	
	N = len(w)
	Pn = {}
	
	#calculating word probability
	for word in w:
		try:
			Pn[word] = Pn[word] + 1
		except:
			Pn[word] = 1
	
	Pd = {}
	for i in range(0, N-n):
		word = ""
		for j in range(i, i+n):
			word = word + w[j] + " "
		Pd[word] = 0
		
	corpus = ""
	for word in w:
		corpus = corpus + word + " "
	
	for key, value in Pd.items():
		Pd[key] = corpus.count(key)/(N)
		
	I = {}
	for key, value in Pd.items():
		w1 = key.split(" ")[0]
		w2 = key.split(" ")[1]
		if(w1 == '.' or w1 == ',' or w2 == '.' or w2 == ','):
			continue
		tagged1= nltk.pos_tag([w1])[0]
		tagged2 = nltk.pos_tag([w1])[0]
		
		if(Pd[key] > 0.008):	
			I[w1 + " " + w2] = Pd[key]
			print(w1 + " " + w2, I[w1 + " " + w2])
	return I	
	
PMI_corpus(w, 2, 5)			
	
	
