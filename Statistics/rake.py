import math
import nltk
from rake_nltk import Rake

targetFile = "../Resource/transcript1.txt"
f = open(targetFile, "r")
data = f.read()
r = Rake()
text="Feature extraction is not that complex. There are many algorithms available that can help you with feature extraction. Rapid Automatic Key Word Extraction is one of those"
r.extract_keywords_from_text(data)

print(r.get_ranked_phrases_with_scores())
