import fasttext
import config
import sys
# Skipgram model :
#model_us = fasttext.train_unsupervised('../Resource/nptl_transcript_1.txt', model='skipgram')

# or, cbow model :
#model_s = fasttext.train_supervised('../Resource/nptl_transcript_1.txt')

#print(len(model_us['the']), model_us)


def word2vec(file_name, model = 'skipgram'):
	vector = fasttext.train_unsupervised(file_name, model='skipgram')
	return vector
	
	
