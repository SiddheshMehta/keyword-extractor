import utils as ut
import math
import nltk

targetFile = "../Resource/transcript1.txt"
w = ut.getTokens(targetFile, stopWords = True)
w.remove(".")

def zipfs(w):
	
	N = len(w)
	frequency = {}
	
	#calculating word probability
	for word in w:
		try:
			frequency[word] = frequency[word] + 1
		except:
			frequency[word] = 1
			
	countList = []
	for key, value in frequency.items():
		countList.append(value)
		
	countList.sort()
	countList = countList[::-1]
	
	rank = {}
	for i in range(0, len(countList)):
		rank[countList[i]] = (i + 1) 
	
	wordWeights = {}
	for key, value in frequency.items():
		wordWeights[key] = frequency[key]*rank[value]
	return wordWeights
		
zipfs(w)	
