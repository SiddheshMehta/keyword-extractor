from nltk.corpus import stopwords

STOPWORD_FILE = "stopwords.txt"

STOPWORDS = set(open(STOPWORD_FILE, "r").read().split("\n") + stopwords.words('english'))

FRAME_COUNT = 10

WORD2VEC_FILE = "/tmp/word2vec.txt"

