from keybert import KeyBERT
import nltk
import fasttext
from nltk.corpus import stopwords
import utils

f = open("../Resource/text_lect1.txt")
doc = f.read()

kw_model = KeyBERT()

sentences = nltk.sent_tokenize(doc)
sentences = utils.preprocess(sentences)
vector = utils.word2vec("../Resource/text_lect1.txt")

keywords = kw_model.extract_keywords(utils.doc_maker(sentences), keyphrase_ngram_range=(1,3), use_mmr = True)


net_sim = 0
#for  i in range(0, len(keywords)):
#	for j in range(i+1, len(keywords)):
#		A = keywords[i][0]
#		B = keywords[j][0]
#		sim = utils.cosine_similarity(vector[A], vector[B])
#		net_sim += sim
print(keywords)
print(len(sentences))
