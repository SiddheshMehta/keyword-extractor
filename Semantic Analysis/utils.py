import fasttext
import nltk
from numba import jit, cuda
import numpy as np
# to measure exec time
from timeit import default_timer as timer  


@jit(target_backend='cuda')
def mod(vector):
	modulus = 0
	for dim in vector:
		modulus = modulus + dim**2
	modulus = modulus**(1/2)
	return modulus

@jit(target_backend='cuda')	
def dot(A, B):
	dot_prod = 0
	for i in range(0, len(A)):
		dot_prod = dot_prod + A[i]*B[i]
	return dot_prod

@jit(target_backend='cuda')
def cosine_similarity(A, B):
	epsilon = 0.0000000000000000000001
	if(mod(A) * mod(B) <= epsilon):
		return 0
	return dot(A, B)/(mod(A) * mod(B))
	
	
def preprocess(sentences):
	output_sentences = []
	for sentence in sentences:
		flag = False
		for ch in sentence:
			try:
				ch = ord(ch)
			except:
				continue
			if( ( ch >= 65 and ch <= 90 ) or ( ch >= 98 and ch <= 122)):
				flag = True
			elif(ch > 255):
				flag = False
				break
			
		sentence = sentence.replace('\n', '', sentence.count("\n"))
		if(flag == True):
			output_sentences.append(sentence)
	return output_sentences
	
def doc_maker(sentences):
	doc = ""
	for sentence in sentences:
		if(sentence[-1] != '.'):
			doc = doc + sentence + " . "
		else:
			doc = doc + sentence + " " 
	return doc
	
	
@jit(target_backend='cuda')
def word2vec(file_name, model = 'skipgram'):
	vector = fasttext.train_unsupervised(file_name, model='skipgram')
	return vector
	
	
def get_representing_sentence(key_phrase, block):
	keywords = nltk.word_tokenize(key_phrase)
	sentences = nltk.sent_tokenize(block)
	match_matrix = {}
	max_count = 0
	max_net_count = 0
	summary = sentences[0]
	for sentence in sentences:
		count = 0
		net_count = 0
		match_matrix[sentence] = {}
		words = nltk.word_tokenize(sentence)
		for keyword in keywords:
			match_matrix[sentence][keyword] = 0	
			for word in words:
				 if(word.lower() == keyword.lower()):
				 	match_matrix[sentence][keyword] += 1
			net_count += match_matrix[sentence][keyword]
			if(match_matrix[sentence][keyword] > 0):
				count += 1
		if(count > max_count):
			max_count = count
			max_net_count = net_count
			summary = sentence
		elif(count == max_count and net_count > max_net_count):
			max_net_count = net_count
			summary = sentence
			
	return summary		
	
@jit(target_backend='cuda')	
def vector_similarity(keywords, vector):
	net_sim = 0
	for  i in range(0, len(keywords)):
		for j in range(i+1, len(keywords)):
			A = keywords[i][0]
			B = keywords[j][0]
			sim = cosine_similarity(vector[A], vector[B])
			net_sim += sim	
	return net_sim

