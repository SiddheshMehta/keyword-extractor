import nltk
import fasttext
from nltk.corpus import stopwords
import matplotlib.pyplot as plt
import numpy as np
import utils

from numba import jit, cuda
import numpy as np
# to measure exec time
from timeit import default_timer as timer   


f = open("../Resource/text_lect1.txt")
data = f.read()
sentences = nltk.sent_tokenize(data)	
	
sentences = utils.preprocess(sentences)

block_size = 5
sent_blocks = []
word_blocks = []

for i in range(0, len(sentences),5):
	end = min(len(sentences), i+5)
	temp = [x for x in sentences[i:end]]
	sent_blocks.append(temp)

for i in range(0, len(sent_blocks)):
	sent_block = sent_blocks[i]
	block = ""
	for b in sent_block:
		block = block + " " + b 
	temp = nltk.word_tokenize(block)
	temp_1 = []
	stop_words = set(stopwords.words('english'))
	for word in temp:
		if word not in stop_words:
			temp_1.append(word)
	word_blocks.append(temp_1)	



@jit(target_backend='cuda')
def get_block_diff(word_blocks):
	vector = utils.word2vec("../Resource/text_lect1.txt")	
	N = len(word_blocks)
	block_diff = []
	for i in range(0, N-1):
		count = 0
		squared_diff = 0
		for word1 in word_blocks[i]:
			for word2 in word_blocks[i+1]:
				if(True):
					diff = 1 - utils.cosine_similarity(vector[word1], vector[word2])
					count += 1
				else:
					pass
				squared_diff += diff**2
		
		mean_squared_diff = (squared_diff)**(1/2)
		block_diff.append(mean_squared_diff)
	
	return block_diff
		
block_diff = get_block_diff(word_blocks)

x = np.array([x+1 for x in range(0, len(block_diff))])
y = np.array(block_diff)
plt.plot(x, y)  
plt.show() 	
	
	
	


