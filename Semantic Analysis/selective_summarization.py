from keybert import KeyBERT
import nltk
import fasttext
from nltk.corpus import stopwords
import utils


def get_selective_summary(doc):	

	kw_model = KeyBERT()

	sentences = nltk.sent_tokenize(doc)
	sentences = utils.preprocess(sentences)


	block_size = 8
	sent_blocks = []
	word_blocks = []

	for i in range(0, len(sentences),block_size):
		end = min(len(sentences), i + block_size)
		temp = [x for x in sentences[i:end]]
		sent_blocks.append(temp)

	summary = []
	for block in sent_blocks:
		doc = utils.doc_maker(block)
		keywords = kw_model.extract_keywords(doc, keyphrase_ngram_range=(1,3), use_mmr = True)
		key_phrase = keywords[0:1][0]
		sentence = utils.get_representing_sentence(str(key_phrase), utils.doc_maker(block))
		summary.append(sentence)
	return summary
		
#summary = utils.doc_maker(get_selective_summary(sent_blocks))
#keywords = kw_model.extract_keywords(summary, keyphrase_ngram_range=(1,3), use_mmr = True)		
#print(keywords)		
		
		
		
		
		
		
	
