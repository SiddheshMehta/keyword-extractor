from keybert import KeyBERT
import nltk
import fasttext
from nltk.corpus import stopwords
import utils
import selective_summarization as ss

kw_model = KeyBERT()

f = open("../Resource/text_lect1.txt")
doc = f.read()
vector = utils.word2vec("../Resource/text_lect1.txt")

#summary = utils.doc_maker(ss.get_selective_summary(doc))
#keywords = kw_model.extract_keywords(doc, keyphrase_ngram_range=(3,3), use_mmr = True)	
	
#print(utils.vector_similarity(keywords, vector))

sent_blocks = []
block_size = 8


sentences = nltk.sent_tokenize(doc)
sentences = utils.preprocess(sentences)

for i in range(0, len(sentences),block_size):
	end = min(len(sentences), i + block_size)
	temp = [x for x in sentences[i:end]]
	sent_blocks.append(temp[0])

for i in range(0, 12): 
	for j in range(i+1, 12):
		doc = utils.doc_maker(sent_blocks[i:j])
		if(j - i >= 3):
			doc = utils.doc_maker(ss.get_selective_summary(doc))
		keywords = kw_model.extract_keywords(doc, keyphrase_ngram_range=(3,3), use_mmr = True)
		sim = utils.vector_similarity(keywords, vector)
		print(int(sim*100), " ", end = "")
	print("")
		
