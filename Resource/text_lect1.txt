Hello all, welcome to this particular course, which is designed for Computational Science in
Engineering just to give you an idea about this particular course we will just talk about what is
preliminary planned for this course and what about. So, myself is Ashoke De, currently
working as Associate Professor in the Department of Aerospace Engineering at IIT, Kanpur.
So, I will be the instructor for this course and what is Computational Science and Engineering?
Essentially this is an evolving field, which actually explodes the power of computational tool
or computational methodologies to solve or to address issues in practical problems, whether it
is in engineering or social sciences or such kind of field. So, the whole idea or the primary idea
is that to use the computational science techniques or the methodologies to solve the problems,
so in a nutshell or essentially, they should talk about or try to frame and framework for solution
methodologies for large scale problems.
And now, when you use the power of computation, obviously it is based on certain primary
things and like a linear algebra or differential equations. So, these are sort of the building blocks
towards those devising those kinds of methodologies.
So, what we are going to cover in this particular course that you can see here, so as I said the
primary focus actually lies on developing the problem-solving methodologies or robust tool for
numerical simulation. And here the idea of the goal is to present the fundamental of numerical
techniques or scientific computing with sort codes, wherever it is required to sort of establish
the key concept.
So, obviously, it includes some frameworks of the applied mathematics such as linear algebra,
what we see here, then differential equations, whether ODEs or PDEs. So, these are the sort of
building blocks, which will allow to look at this computing methodologies and then to solve a
problem or address the scientific or engineering problem. So that essentially, it would lead to
some sort of a skill set, which you will develop for mathematical modelling based on
computational technique.
So, as you see that initial part, we will be talking about like some of these applied mathematical
algebras, which will include linear algebra and where we will talk about all these different
vectors, vector space, Eigen values, orthogonality, single value decompositions and all such
things. Then talk about little bit of ODEs and then PDEs, but then we will move to this part,
where we will be talking about all these numerical methods.
And as you see that we will talk about in details about solving solution of the linear system,
and different issues related to that here, when you will come to here and then ODEs. One may
immediately think about that is nothing mentioned about PDEs, we will just talk about a little
bit but not too much, because when you talk about or devise some techniques for PDEs, these
actually lead to a typical some of the dedicated core future in terms of computational fluid
dynamics CFD or compressible fluid mechanics whatever it is, to that short will not go into the
detailed discussion of any formulations for PDEs.
And these are some of the suggested books which would be kind of useful like the linear algebra
part is very much, one can follow the Gilbert Strang or any engineering mathematics book.
Then these are for differential equations and then the couple of numerical techniques, books or
scientific computing books and then some computational fluid mechanics books which we will
talk about and all these. So, that is what pretty much what you can expect that what we will be
talking about in this particular course.
So, let us start with the discussion and first thing that as we mentioned that we will start with
the linear algebra. So, what linear algebra does that it actually deals? So, one can say linear
algebra it deals with linear system. So, which is not essentially a linear equation and its
behaviour or characteristics which may include nature of solution, existence of solution and
possible ways to obtain the solution, so that is what linear algebra does.So, what if you look at that, if my linear algebra if you talk about so, this will deal with some
linear system so, let us say the behaviour of that and then one can look at the uniqueness of
solution or other way one can look at the existence of the solution. So, which will lead to like
nature of solution and obviously then how to obtain solution? So, how you deal with the things,
so essentially what you have is a physical system.
So, that means, there are certain quantities or variables which are associated with that, which
will lead to some sort of and governing equations, which are kind of going to represent that
problem. So, here now simplification of governing equations into linear system for obtaining
the so, basically one can think about the simplification of governing equation into linear system
for obtaining solution easily. So, what do we look at it that we will look at different things like
row picture, column picture, matrix form so, these are the things which will come on the way.
Now, let us start the discussion within simple example so, let us take an example so, let us say
my system is 2𝑥 − 𝑦 = 0, −𝑥 + 2𝑦 = 0. So, now, if we convert that to a linear system, so the
linear system of this one can write like
2
[
−1
0
−1 𝑥
] { 𝑦} = [ ]
3
2
So, if we change that, then we can write in that way now, what we can look at first is the row
picture as we mentioned. So, how do we look at it so that means we are going by this is the
row, so we will go as a row picture.
So, let us draw this and so when I do that what it happens that this is (2, -1) and so they should
be going through 3 and then so like this, this guy will pass through. So, this is −𝑥 + 2𝑦 = 3.So, this is what it goes through, then the other guy goes through from here, which would be
show through this line, which is 2𝑥 − 𝑦 = 0. So, this should be (1, 2) so we will go by that
means this is first row, this is second row so this is how it looks like.
Now, if you on the other hand look at the column picture, so column picture we can rewrite
this guy as
0
2
−1
𝑥[ ]+𝑦[ ] = [ ]
3
−1
2
So, from column picture, it says that from some linear combination of two left hand side
columns will give the columns of the right-hand side. So, what it is that
0
𝐶 1 + 2𝐶 2 = [ ]
3
Again
−𝐶 1 + 2𝐶 2 = [
−4
]
5
So, you can see this is 𝐶 1 , should these guys 𝐶 1 this is 𝐶 2 , so if it is 𝐶 1 + 2𝐶 2 , so that gives me
(0, 3).
And if I take in a different combination, so these are the column vectors, which are going to do
like this. So, when I plot this guy in a column picture, so this would be interesting to see like
when we do that, so the first column is (2, -1), so this is (2, -1). So, this is (2, -1) or sort of 𝐶 1
or this is the vector of 𝐶 1 and the second one is (-1, 2), so this is 𝐶 2 so (-1, 2) that 𝐶 2 . |Now
0
what you say 𝐶 1 + 2𝐶 2 = [ ]
3
So that means comes here, so this is 𝐶 1 and then 2𝐶 2 , if we go that means we will go parallel
to this, like this and then we go another 𝐶 2 , so this is 𝐶 2 this is 𝐶 2 . So, we come here, (0, 3). So
that means when I add this column vector, this is 𝐶 1 then 2𝐶 2 along this direction that gives me
(0, 3). Similarly, when you do this guy −𝐶 1 + 2𝐶 2 , so this is (-4, 5), so it should be here. So,
𝐶 2 it goes like another 𝐶 2 then −𝐶 1 , so it goes like that.
So, this is −𝐶 1 , this is 𝐶 2 so this is (-4, 5) so this is how the column picture actually behaves.
So, the column picture does basically deal with the linear combinations. For 2D case, this
solution was available through both row and column picture, but when you go to 3D, so that
things become a bit complicated. So, let us take an example on 3D.
So, let us say we will consider
2𝑥 − 𝑦 = 0
−𝑥 + 2𝑦 − 𝑧 = −1
−3𝑦 + 4𝑧 = 4
So, now if we write this, this should be written as
2 −1 0 𝑥
0
[ −1 2 −1 ] { 𝑦 } = [ −1 ]
0 −3 4 𝑧
4
So, essentially, one can think about this is A, this is x, this is b, so, that gives me Ax = b. Now
here the row picture if we see the row picture, so, each of the 3 equations denotes 3 planes, so
these are planes. So, each row is 3 planes and if the solution exists it will be the point of
intersection of 3 planes. So that means so the solution if exists will be the point of intersection
of 3 planes.
Thus, however, it is hard to visualize, so this is in 3 dimensions so, it would be have to visualize.
Hence, row picture is not an appropriate solution essentially that is what it is that row picture
is not an appropriate for a solution if number of unknowns increase or row increases. So, this
is not a sort of and easiest way to look at things, so rather to try to look at from the column
picture.
Now, what column picture tells us that this guy can be written as
2
−1
0
0
𝑥 [ −1 ] + 𝑦 [ 2 ] + 𝑧 [ −1 ] = { −1 }
0
−3
4
4So, here again if someone at least in 3 dimensions draw it, so, this is x y, z. So, this would be
essentially you somewhere 2, so somewhere C 1 , somewhere this would be C 2 , we somewhere
here like this. Now another important thing is that these column vectors are not parallel and
also, they are not arranged as 1 as the sum of the other 2.
So, hence these guys, these, these and these they are sort of independent vectors. there also
important to look at, they are also not in the same plane. Here the solution will be the final
solution would be (0, 0, 1). Now, these kinds of graphic visualizations become impractical for
more than 3 dimensions because that is where you cannot visualize, but the unknowns if they
are more than 3 or it increases, then even the column picture or graphical visualization become
difficult.
So, the point here is that we have to come up with other ways to find out so, now the point Ax
= b, now if a solution exists for possible b’s. So, this system Ax = b will provide a solution or
the solution exists for possible b’s only if all column vectors are linearly independent and also
these vectors will span throughout the space. Now, the b column vectors produce 3 different
planes in a 3D space and hence, have solution for any b.
However, if number of planes produced is less than the space dimension, then the solution may
exist only for specific b’s, so that is a very important thing to note. Now, we will look at how
we do so the Gauss Elimination, what we does? In the Gauss Elimination, so this would be alsoassociated with back substitution elementary matrix. so, these are sort of associated with that
substitution elementary matrix etcetera.
Again, here also we will try to look at the things through examples again, we will take an
example
𝑥 + 2𝑦 + 𝑧 = 2
3𝑥 + 8𝑦 + 𝑧 = 12
4𝑦 + 𝑧 = 2
So, if we write them down
1 2
[ 3 8
0 4
1 𝑥
2
1 ] { 𝑦 } = [ 12 ]
1 𝑧
2
so this is here A, this is x, this is b. Now here in the Gauss elimination process, we will do or
it is done by row elimination. So, what we will do here? We will do
𝑅 2 → 𝑅 2 − 3𝑅 1
so that will give us
2 1 𝑥
2
2 −2 ] { 𝑦 } = [ 6 ]
4 1 𝑧
2
1
[ 0
0
Now the second stage, what we will do,
𝑅 3 → 𝑅 3 − 2𝑅 2
and we will get
1
[ 0
0
2 1 𝑥
2
𝑦
]
{
}
=
[
2 −2
6 ]
0 5 𝑧
−10
So, here what we get here this if you look at you have the diagonal elements setting here, now,
this is a 3×3 matrix and the below diagonal elements are missing or kind of eliminated. So,
this is an upper triangular matrix and what we get the solution is received by the back
substitution.
Now, the point is that if the first pivot, so this is the first pivot, if the first pivot is 0 then rows
are exchanged, Gauss elimination fails to give solution when all pivots are 0. So, this is one of
the limitations that one has to keep in mind that if that happens then no one would be do. Now
upper row operations what do we get we get an upper triangular matrix UX equals to let us say
C, so this is what you get this is a new vector C.
So, U is upper triangular matrix here what we get that this will now the row vector multiplied
with the matrix will give a row vector for example, we do like for example,
[1 2
1 2 1
1
5] [ 3 8 1 ] → [1 [ 3 ]
0 4 1
0
2
2 [ 8 ]
4
1
5 [ 1 ]] = [7 38
1
8]
1 2 1
Now, we come to the elementary matrix, what is that now we have this A which is [ 3 8 1 ],
0 4 1
1 2 1
so here after row operation we got [ 0 2 −2 ]. So, this is what we got to the first-row
0 4 1
operation that we did. So, this could be multiplied with like then what we can say this is my
1 2 1
1 2
original system [ 3 8 1 ] which got into this [ 0 2
0 4 1
0 4
1
−2 ]. So, here is the matrix which got
1
multiplied and this is called the elementary matrix of E 21 .
So, what did that we have done R 2 - 3R 1 , so here this would be1 0 0
[ −3 1 0 ]
0 0 0
So, this is what it does the elementary matrix, so when you do that, this is row operation so this
should be 1, so you get back these things. So, we will see that when you have this row operation
instead of doing that you can have an elementary matrix which is multiplied with this original
matrix and you get the other one. So, we will continue this discussion in the next session.

So, we are looking at the Gauss Elimination process, and we just started discussing about the
elementary matrix and let us continue what we were talking about the elementary matrix here.
So, here you can see when you do the row operation so, the process actually what you have
done. This is the row operation that the first row operation that we have done. And that is why
you get the first second row and then when you do the operation of the third row that is what
you get it. So, now what we can do?
We can similarly find out these things by doing row operations of the system. So, this is the
matrix and when you did that operation, first row operation we get this and so, this is the
elementary matrix which 𝐸 21 . So, that is what it gives the first things. So, this is 𝐸 21 . So, what
we can have essentially that means the other one would be 𝐸 32 (𝐸 21 . 𝐴) = 𝑈. which will give
us the upper triangular matrix or one can write 𝐸 32 (𝐸 21 . 𝐴) = 𝑈 = (𝐸 32 . 𝐸 21 )𝐴. So, this is
called the associative property of the matrix.
So, now, the other matrix which will come on the way is the permutation matrix, so what is the
𝑎 𝑏
permutation matrix? It just like let us say we have [
], so it can be multiplied with the
𝑐 𝑑
𝑐 𝑑
permutation matrix, so, that will get [
]. So, let us say we can then multiply to these another
𝑎 𝑏
0 1
2× 2 system, which could be [
]. So, this guy is called the permutation matrix. Now, this
1 0
can be multiplied on the other side of the system for example,
𝑎
[
𝑐
𝑏 0
][
𝑑 1
1
𝑏
]=[
0
𝑑
𝑎
]
𝑐
So, point here is that the left hand multiplication changes row and right hand multiplication
changes columns. So, this is important here that left hand multiplication changes rows and right
hand multiplication changes columns. So, this follows non commutative nature of matrix
multiplication which is AB ≠ BA, this is called the non-commutative nature of matrix
multiplication.So, the process of elementary matrix generally goes from left to right, which makes it easier to
find the inverse. For like we had 𝐸 32 . 𝐸 21 . 𝐴 = 𝑈 is upper triangular matrix. So, if we try to
find out A this would be A,
𝐴 = 𝐸 21 −1 𝐸 32 −1 𝑈
So, inverse will exist as all column vectors are linearly independent, this is important inverse
will exist if all column vectors are linearly independent and we have seen through some simple
examples of 2 dimension and 3 dimensions, how they span whether they kind of independent
or not.
So, in that case what will happen is that 𝐴. 𝐴 −1 = 𝐼 identity matrix. So, what we will write
𝐸 21 −1 𝐸 32 −1 𝑈 ∙ 𝐴 −1 = 𝐼
So, A inverse would be
𝐴 −1 = 𝑈 −1 𝐸 32 . 𝐸 21
So, this is how you can find out that thing
1 0
Or other hand you had, so our E 21 was [ −3 1
0 0
0
−1
0 ], this is our E 21 . Now, to get the 𝐸 21
0
which would be straightforward,
1
[ 3
0
0 0
1 0 ]
0 1
this will give us an identity matrix.
1 0 0 1 0
[ 3 1 0 ] [ −3 1
0 0 1 0 0
0
1 0
0 ] = [ 0 1
0
0 0
0
0 ]
1So, you can see this is how it works. Now, we look at the matrix multiplication. So, matrix
multiplication what we do is that
[𝐴] 𝑛×𝑚 [𝐵] 𝑚×𝑝 = [𝐶] 𝑛×𝑝
So, least number of conditions that need to be satisfied:
i) Number of columns of A would be number of rows of B.
ii) 𝐶 𝑖𝑗 = ∑ 𝑚
𝑘=1 𝑎 𝑖𝑘 𝑏 𝑘𝑗
Other possibilities to do matrix multiplications column multiplications approach where let us
say let B 1 , B 2 ,… B p are columns of B and C 1 , C 2 ,… C p are columns of C. Then we can write
𝐴 ∙ 𝐵 1 = 𝐶 1 , 𝐴 ∙ 𝐵 2 = 𝐶 2 that means 𝐴 ∙ 𝐵 𝑖 = 𝐶 𝑖 in generic form one can write. So, here are the
columns of C or some linear combination of columns of A.
Now if we go row multiplication approach which is like let us say A i where i goes 1 2 to n
these are the rows of A. Then we can write 𝐴 ∙ 𝐵 𝑖 = 𝐶 𝑖 where i goes 1 to n where 𝐶 𝑖 is the ith
row in C matrix. So, you can see rows of C are linear combination of rows of B. Now if you
let us say columns of A and rows of B, that is the way if you multiply it, so like
4
1
−1 3
0 −8
−1 −5
[ 2 ] [−3 1] + [ 5 ] [0 −2] = [ −2 6 ] + [ 0 −10 ] = [ −2 −4 ]
3
6
−3 9
0 −12
−3 −3
So, that means C would be summation
𝑚
𝐶 = ∑ 𝐴 𝑖 𝐵 𝑖
𝑖=1where 𝐴 𝑖 ith column of A, 𝐵 𝑖 is ith rows of B. So, now, there are other means that one can find
out is the block multiplication approach.
So, here in the block multiplication approach, you can do the multiplication in that fashion in
block wise like
[
𝐴 1
𝐴 3
𝐴 2 𝐵 1
][
𝐴 4 𝐵 3
𝐴 𝐵 + 𝐴 2 𝐵 3
𝐵 2
]=[ 1 1
𝐵 4
𝐴 3 𝐵 1 + 𝐴 4 𝐵 3
𝐴 1 𝐵 2 + 𝐴 2 𝐵 4
]
𝐴 3 𝐵 2 + 𝐴 4 𝐵 4
So, now, this is what you call it a block multiplication. Now, if we talk about inverse of A
matrix there are in general there are 2 inverses one is the right inverse which we call like
𝐴 𝑟 −1 𝐴 = 𝐼 is identity where 𝐴 𝑟 −1 is the right inverse.
Or you can have left inverse where 𝐴 𝑙 −1 𝐴 = 𝐼 is identity where 𝐴 𝑙 −1 is left inverse. Now, one
can note here for symmetric matrix would the inverses are same. So, for symmetric matrix
𝐴 𝑟 −1 = 𝐴 𝑙 −1 . Now, when you talk about inverse, so, there will be another question which may
come how do we guarantee that inverse actually exists. So, one can say that inverse exists if
and only if number
i) determinant is not 0 that means is not equal to 0.
ii) all columns are linearly independent.
Now, for a non-zero vector x if Ax = 0 then the matrix is not invertible. So, this is what that
means the columns are not linearly independent. So, you can take an example like let us say
1
𝐴=[
2
3
]
6So, there exists A vector which will like 𝑋 = [
3
], then if we do 𝐴𝑥 = 0, so which means A
−1
inverse does not exist. So, now, if Ax = 0 then 𝐴 −1 𝐴𝑥 would be also 0 which means x is 0. So,
the main logic is that x has to be a non zero vector. So, that is what is important.
Now, one can say how to find 𝐴 −1 that is important, so let us take an example again for where
the matrix is invertible. So, let us say
1
𝐴=[
3
2
]
7
So, here the matrix is invertible then we can find out the inverse and let us say the inverse is in
𝑎 𝑏
]. So, we can write
𝑐 𝑑
1 0
𝑎 𝑏 1 2
[
][
]=[
]
0 1
𝑐 𝑑 3 7
terms of A matrix which is 𝐴 −1 = [
which would be identity matrix. So, just using the property of the matrix that inverse A equal
to identity.
So, from here what you get
𝑎 + 2𝑏 = 1
𝑐 + 2𝑑 = 0
3𝑎 + 7𝑏 = 0
3𝑐 + 7𝑑 = 1
𝑏=−
𝑎−
3𝑎
7
6𝑎
=1
7𝑎=7
𝑏 = −3
𝑐 = −2
𝑑=1
So, you can solve this is a simple algebra one can solve it and you can get these things. So, the
1
𝑏
1
] if this is multiplied with [ ] first column this will get us [ ] and
0
2
𝑑
3
1
𝑏
] if it is multiplied with [ ] this should be [ ]. So, this is what it actually does. Now,
0
7
𝑑
point here is that if [
[
𝑎
𝑐
𝑎
𝑐
how do we get back that this is one way to do that.
Now, there is alternative way one can also handle the situation like let us see if you augment
the system. So, we augment the system what do we have here [
1 3 1
⋮
2 7 0
0
]. So, augment the
1
identity matrix here and we do row operation which is let us see R 3 -3R 1 . So, here what we get
1 3 1 0
1 0 7 −3
⋮
] then again, we do R 1 -3R 2 . So, we get [
⋮
], so which means,
0 1 −2 1
0 1 −2 1
one can write here [𝐴 𝐼 ] = [𝐼 𝑋]. So, if I both side multiplied with A inverse, so this is
[
𝐴 −1 [𝐴
𝐼 ] = 𝐴 −1 [𝐼
𝑋]
that would give us.
[𝐼
𝐴 −1 ] = [𝐴 −1
𝐴 −1 𝑋]So, what we have got here this is the inverse of the system and you can see when we do the
other way around it is the same. So, what are the elementary operations that has been carried
out, the elementary operations which are R 2 - 2R 1 and R 1 – 3R 2 . So, this guy then my E 21 would
be [
1 −3
1 0
] and E 12 would be [
]. So, these are my elementary matrices.
0 1
−2 1
So, my upper triangular matrix U would be, 𝑈 = 𝐸 21 𝐴. So, 𝐸 12 𝐸 21 𝐴 = 𝐼, So, 𝐴 −1 = 𝐸 12 𝐸 21 .
So, this is how you can also so, if you multiply the so, this is an important property and one
can cross check here if you multiplied this is 1 2. So, this will give us A inverse. So, if you
multiply the elementary matrices that also give you the inverse. Now, there are some
characteristics properties which are there,
i) (𝐴𝐵) −1 = 𝐵 −1 𝐴 −1 which means 𝐵 −1 𝐴 −1 𝐴𝐵 = 𝐼.
And 𝐴 −1 𝐵 −1 𝐴𝐵 ≠ 𝐼.
so provided A is invertible it can be denoted as product of lower triangular and upper triangular
matrices. So, we can look at the same thing from the previous example.
1 0
], so you can see that and then upper
2 1
1 3
1 3
triangular is [
] this is L this is U not only give us [
] which is A. Now, what is L?
0 1
2 7
So, we had lower triangular matrix which is [
𝐿=[
1 0
1 0 −1
]=[
] = 𝐸 21 −1
2 1
−2 1
So, this is my elementary matrix 𝐸 21 . So, I can write 𝐴 = 𝐸 21 −1 𝑈. So, my L would be 𝐸 21 −1
is the lower triangular matrix.Now, an invertible matrix can be decomposed into a lower triangular, diagonal matrix and
upper triangular matrix. Let us say we have this guy like the same example here, we have
2 3
1 0 2 3
1 0 2 0 1 3/2
[
][
]=[
]=[
][
][
]
2 1 0 1
4 7
2 1 0 1 0
1
So, this should be lower triangular matrix, this is diagonal matrix and this is upper triangular
matrix, so what essentially one is writing A is L D U. Now, here again little bit more algebra
one can do which will get you [
2 0 1
][
4 1 0
3/2
] this guy is L D, this is U. So, the conservative
1
solutions they do not work for millions of unknowns. So, thus for those kinds of problems,
these kinds of decompositions are highly required because we cannot do this kind of
conservative approach.
So, for example, if you look at the operations of a 3× 3 system, what you get? You get here is
A then finally, lower triangular to upper triangular matrix. So, first thing would be elementary
matrix of 𝐸 21 then elementary matrix of 𝐸 31 then elementary matrix of 𝐸 32 . So, this is what is
going to give us U. What it will give us A would be now, to find out A we get
𝐴 = (𝐸 21 −1 𝐸 31 −1 𝐸 32 −1 )𝑈
So, this one can think about is the lower triangular system for 3× 3 matrix system.
Now, if you have 4× 4 systems then our system then the lower triangular would be you can
think about the lower triangular like first would be
𝐿 = (𝐸 43 𝐸 42 𝐸 32 𝐸 41 𝐸 31 𝐸 21 ) −1So, now, you see when there is an increase in the unknown so, the matrix sizes also go up and
when the matrix sizes go up so, this lower and upper triangular decompositions are important.
So, the point here one has to note that when the system becomes really large or a number of
unknowns are quite high, then the conservative decompositions actually is not a very viable
solution. And either this kind of decomposition is through lower triangular and upper triangular
matrix is quite handy and this is what is desired. And this exactly this situation you will see
when we will be looking at the numerical point of view, how to decompose the matrix and get
these systems and how to get actually the solution. So, we will stop the discussion here and
continue the discussion in the next session.
