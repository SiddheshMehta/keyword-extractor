Initially we started by reading research papers related to keyword extraction. There upon we got to know some useful methods like graph based keyword extraction, using PMI method, conforming zipfs law and rule based keyword extraction.
PMI :
	In PMI we calculate probability of two words coming together in the transcript document. Word pairs with highest proobability are assigned as most probable keywords.
	P(w1, w2) is the probability of the two words coming together.
	Input is a transcript file and output is a dictionary representing key as words from transcript and value as their respective PMI score.
	Implementation:
		File name : PMI.py
	
Graph based keyword extraction:
	Here we calculated following measures for each of the word present in the corpus.
	1. Clustering coefficient
	2. Indegree centrality
	3. Outdegree centrality
	4. Degree centrality
	5. Strength
	6. Strength in
	7. Strength out
	8. Selectivity
	9. Selectivity in 
	10. Selectivity out
	11. Closeness centrality
	12. Betweenness centrality
	A transcript is supplied as input and csv file is generated as output where statistics for each word as stored in each row.
	It is observed that clustering coefficient, Closeness centrality and betweenness centrality are strong measures of 
	measuring centrality.
	Advantages:
		Selectivity measure is most accurate to get keyword related words.
	Disadvantages and flaws:
		Could not find phrase level keywords
		Large quantity of noise is observed. Could not detect non-contectual keywords.
	Implementation:
		Implemented a syntax graph in python. It is a directed graph. Edges have weights which denote number of times any two 
		words come in a specific order. Nodes have label as words. Statistic information regarding each node is stored in it.
		Here we used nltk, nltk.stopwords and WordNetLemmatizer
		File name : Graph.py
	
Rule based keyword extraction:
	This approach is encouraged by MEM and CRF features.
	Here we defined harded coded features. Each feature has filters. When a transcript is passed through a feature, then the 
	feature filters irrelevant sentences from the transcript and selects only relevant ones. Then RAKE is applied through the 
	selected sentences and most important phrases are selected as possible keywords.
	Advantages:
		Easy to implement.
		Gives most accurate result.
		Computationally inexpensive.
	Disadvantages:
		Fixed features.
		Selecting and defining features is difficult.
		Cannot include cases that are not included in the features.
	File name : k_rules.py
		
Other implementations:
	Implemented zipfs law, studied and implemented word2vec and used RAKE.
	
Progress:
	Able to recognize some of contectual keywords.
	Assign statistic values to words of corpus via different measures.
	Implemented different aglorithms from different research papers

Difficulties:
	Could not find non-contextual keywords.
	Could not track relevant information out of the transcript.
	 
